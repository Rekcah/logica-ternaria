import re

def matchExpressionBetweenBrackets(input_expression):
    ####Controlli sull'input
    # Se non riscontra valori in input
    exit_commands = ['exit', "e", "quit", "q"]
    help_commands = ["help", "h"]
    if not input_expression:
        return
    elif input_expression in exit_commands:
        print("Uscita...")
        exit()
    elif input_expression in help_commands:
        help = """
            ### AIUTO ###
            Questo è il programma della logica ternaria nella quale si utilizzano dunque tre valori:
            V = vero
            F = falso
            N = nullo, non apofantico
            Considera che il concetto positivo è un atto determinato che determina, è qualcosa che esiste e che
            per questo consente di dotare del valore vero le proposizioni. Dire "non rosso" equivale a dire un concetto
            negativo corrispondente al falso (F). È una creatura della logica, poichè nessuna cosa del mondo è non rossa.
            Questo perché le proprietà si definiscono sempre di qualcosa.
            Esempio: la mela è rossa, il non rosso invece si riferisce a qualcosa di un altro colore, mettiamo blu, oppure
            a qualcosa che un colore non lo ha? Ecco perché il non rosso non si applica a nulla. Il non rosso non è una
            proprietà, perchè una proprietà è per definizione sia determinata che determinabile: l'essere rosso di una mela è
            lo stesso essere rosso di una rosa, invece l'essere non rosso dell'insieme dei numeri primi non è la stessa
            cosa dell'essere non rosso dell'arancia, o della banana. Allo stesso modo, l'essere non rosso di un salto è 
            diverso dai due precedenti, l'essere non rosso di una crisi finanziaria, di un teorema matematico o di un
            romanzo, sono tra loro tutti diversi, perché hanno proprietà diverse e le proprietà che hanno determinano come
            sono. Per cui per le proprietà c'è un modo univoco di esistere, per i concetti negativi no. È per questo che sono
            indeterminati. E che diciamo di non-non rosso? Siccome i concetti negativi sono indeterminati e ciò che è di loro
            determinato lo si deve a ciò che hanno negato all'affermazione, la negazione di un concetto negativo è applicabile
            a ogni cosa. La mela è non non rossa, lo è il Sole, lo è l'insieme dei numeri primi e così via. Questo perché
            la negazione di un concetto negativo nega quanto la negazione del positivo affermava. E ciò che nega è il poter
            essere non rosso. Quindi afferma che si può essere non-non rossi, e che quindi non si ha la proprietà di essere
            non-rossi. Ma siccome essere non-rosso non è una proprietà, nessuno ha la proprietà di essere non rosso, e quindi
            tutti sono non-non rossi. Ecco che c'è una forte analogia col concetto di ente, dove tutto è un ente, perché nulla può
            essere un non-ente. E ora, la negazione della negazione di un concetto negativo è il concetto negativo, perché
            il non-non rosso è predicabile di tutti, è la negazione di ciò che è ppredicabile di tutti è qualcosa che non si può
            predicare di nessuno, proprio ciò che è un concetto negativo. E perché il concetto negativo precedente e non uno nuovo?
            Perché per definizione un concetto negativo è la negazione di una affermazione. Mentre il non-non rosso è la negazione
            di una negazione. E ciò restituisce semplicemente il non rosso.
            Analogamente:
            V = essere = rosso.
            F = io = non rosso.
            N = ente = non-non rosso = sistema impossibile.
            
            Simboli logici:
            ( = apre una parentesi.
            ) = chiude una parentesi.
            ^ = and, "E" logico, congiunzione, compressione.
            v = or, "O" logico, disgiunzione, estensione.
            ! = not, "NON" logico, dissoluzione.
            → = implication, implicazione, espansione.
            - = coimplication, coimplicazione, doppia implicazione, congiunzione tra due implicazioni.
            x = xor, eor, exor, exclusive or, disgiunzione esclusiva.
            
            Valori logici accettabili:
            A = calcola l'espressione per A uguale a vero, falso e nullo.
            B = calcola l'espressione per B uguale a vero, falso e nullo.
            V = vero.
            F = falso = negazione del vero o negazione del nullo.
            N = nullo = negazione del falso = doppia negazione del vero.
            
            Comandi:
            e, exit, q, quit = esce dal programma.
            h, help = mostra questa guida.
        """
        return help
    # Se riscontra solo un carattere negli input
    elif len(input_expression) == 1 and input_expression in ['A', 'B', 'V', 'F', 'N']:
        if input_expression == "A":
            return A
        elif input_expression == "B":
            return B
        elif input_expression == "V":
            return "V"
        elif input_expression == "F":
            return "F"
        elif input_expression == "N":
            return "N"

    ####Operazioni sulle parentesi
    # Se riscontra parentesi aggiunge a
    expression = input_expression
    while bool(re.search('(\(([^()]|)*\))', input_expression)) is True:
        match_expression_in_brackets = re.search('(\(([^()]|)*\))', input_expression)
        bracket_index = {'start': match_expression_in_brackets.start(), 'end': match_expression_in_brackets.end()}
        # Se trova parentesi vuote valorizza expression come vuoto
        if match_expression_in_brackets.group() == "()":
            print("Errore: sono state riscontrate delle parentesi senza operazioni valide!")
            exit(1)
            
        # Valorizza expression al valore trovato tra le parentesi
        else:
            expression = match_expression_in_brackets.group()

        if "^" not in expression and "!" not in expression and "v" not in expression and len(expression) == 3:
            expression = expression.replace("(", "")
            expression = expression.replace(")", "")
            if expression == "A":
                input_expression = input_expression.replace(
                    input_expression[bracket_index['start']:bracket_index['end']], A, 1)
                expression = input_expression
            elif expression == "B":
                input_expression = input_expression.replace(
                    input_expression[bracket_index['start']:bracket_index['end']], B, 1)
                expression = input_expression
            elif expression == "V":
                input_expression = input_expression.replace(
                    input_expression[bracket_index['start']:bracket_index['end']], "V", 1)
                expression = input_expression
            elif expression == "F":
                input_expression = input_expression.replace(
                    input_expression[bracket_index['start']:bracket_index['end']], "F", 1)
                expression = input_expression
            elif expression == "N":
                input_expression = input_expression.replace(
                    input_expression[bracket_index['start']:bracket_index['end']], "N", 1)
                input_expression = matchExpressionBetweenBrackets(input_expression)
                expression = input_expression

        ####Calcolo logico dentro le parentesi
        if bool(check_negations(expression)) is True:
            expression = check_negations(expression)

        while "^" in expression:
            expression = match_AND(expression)
        while "v" in expression:
            expression = match_OR(expression)
        while "→" in expression:
            expression = match_IMPLICATION(expression)
        while "-" in expression:
            expression = match_COIMPLICATION(expression)
        while "x" in expression:
            expression = match_EXOR(expression)

        if len(expression) == 3:
            expression = expression.replace("(", "")
            expression = expression.replace(")", "")
            if len(expression) == 1:
                if expression == "A":
                    input_expression = input_expression.replace(
                        input_expression[bracket_index['start'] + 1:bracket_index['end'] - 1], A, 1)
                elif expression == "B":
                    input_expression = input_expression.replace(
                        input_expression[bracket_index['start'] + 1:bracket_index['end'] - 1], B, 1)
                elif expression == "V":
                    input_expression = input_expression.replace(
                        input_expression[bracket_index['start'] + 1:bracket_index['end'] - 1], "V", 1)
                elif expression == "F":
                    input_expression = input_expression.replace(
                        input_expression[bracket_index['start'] + 1:bracket_index['end'] - 1], "F", 1)
                elif expression == "N":
                    input_expression = input_expression.replace(
                        input_expression[bracket_index['start'] + 1:bracket_index['end'] - 1], "N", 1)

            elif len(expression) == 2:
                pass

            else:
                print("Errore: le operazioni sono state effettuate per " + input_expression[
                                                                           bracket_index['start'] + 1:bracket_index[
                                                                                                          'end'] - 1] + " ma il risultato atteso è " + expression)
                exit(1)

    if bool(check_negations(expression)) is True:
        expression = check_negations(expression)
    while "^" in expression:
        expression = match_AND(expression)
    while "v" in expression:
        expression = match_OR(expression)
    while "→" in expression:
        expression = match_IMPLICATION(expression)
    while "-" in expression:
        expression = match_COIMPLICATION(expression)
    while "x" in expression:
        expression = match_EXOR(expression)

    # Verifica che sia rimasto un solo carattere per expression (caso senza parentesi)
    if bool(re.search('(\(([^()]|)*\))', input_expression)) is False:
        if len(input_expression) == 1:
            if expression == "A":
                return A
            elif expression == "B":
                return B
            else:
                return expression
        else:
            return expression
    matchExpressionBetweenBrackets(input_expression)


def check_negations(check_negation_input):
    match_negations = re.findall('(?<=!).', check_negation_input)
    if bool(match_negations) is True:
        result_list = []
        n = []
        for match in re.finditer('(?<=!).', check_negation_input):
            n.append(match.start())
        for match_to_negate in match_negations:
            if match_to_negate:
                if match_to_negate == "A":
                    if A == "V":
                        result_of_negation = "F"
                        result_list.append(result_of_negation)
                    elif A == "F":
                        result_of_negation = "N"
                        result_list.append(result_of_negation)
                    elif A == "N":
                        result_of_negation = "F"
                        result_list.append(result_of_negation)
                elif match_to_negate == "B":
                    if B == "V":
                        result_of_negation = "F"
                        result_list.append(result_of_negation)
                    elif B == "F":
                        result_of_negation = "N"
                        result_list.append(result_of_negation)
                    elif B == "N":
                        result_of_negation = "F"
                        result_list.append(result_of_negation)
                elif match_to_negate == "V":
                    result_of_negation = "F"
                    result_list.append(result_of_negation)
                elif match_to_negate == "F":
                    result_of_negation = "N"
                    result_list.append(result_of_negation)
                elif match_to_negate == "N":
                    result_of_negation = "F"
                    result_list.append(result_of_negation)
                else:
                    print("Errore: si è inserita l'operazione invalida", match_to_negate, "dopo la negazione.")
                    exit(1)
        new_input = list(check_negation_input)
        for index, result in zip(n, result_list):
            new_input[index] = result
            output = "".join(new_input).replace("!", "")
        return output
    else:
        return False


def match_AND(input_AND):
    match_AND = re.findall('([^\^])\^([^\^])', input_AND)
    result_AND = ""
    if match_AND[0][0] == "A":
        if A == "V":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "V"
                elif A == "F":
                    result_AND = "F"
                elif A == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "V"
                elif B == "F":
                    result_AND = "F"
                elif B == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "V":
                result_AND = "V"
            elif match_AND[0][1] == "F":
                result_AND = "F"
            elif match_AND[0][1] == "N":
                result_AND = "F"
        elif A == "F":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "F"
                elif A == "F":
                    result_AND = "F"
                elif A == "N":
                    result_AND = "N"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "F"
                elif B == "F":
                    result_AND = "F"
                elif B == "N":
                    result_AND = "N"
            elif match_AND[0][1] == "V":
                result_AND = "F"
            elif match_AND[0][1] == "F":
                result_AND = "F"
            elif match_AND[0][1] == "N":
                result_AND = "N"
        elif A == "N":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "F"
                elif A == "F":
                    result_AND = "N"
                elif A == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "F"
                elif B == "F":
                    result_AND = "N"
                elif B == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "V":
                result_AND = "F"
            elif match_AND[0][1] == "F":
                result_AND = "N"
            elif match_AND[0][1] == "N":
                result_AND = "F"
    elif match_AND[0][0] == "B":
        if B == "V":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "V"
                elif A == "F":
                    result_AND = "F"
                elif A == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "V"
                elif B == "F":
                    result_AND = "F"
                elif B == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "V":
                result_AND = "V"
            elif match_AND[0][1] == "F":
                result_AND = "F"
            elif match_AND[0][1] == "N":
                result_AND = "F"
        elif B == "F":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "F"
                elif A == "F":
                    result_AND = "F"
                elif A == "N":
                    result_AND = "N"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "F"
                elif B == "F":
                    result_AND = "F"
                elif B == "N":
                    result_AND = "N"
            elif match_AND[0][1] == "V":
                result_AND = "F"
            elif match_AND[0][1] == "F":
                result_AND = "F"
            elif match_AND[0][1] == "N":
                result_AND = "N"
        elif B == "N":
            if match_AND[0][1] == "A":
                if A == "V":
                    result_AND = "F"
                elif A == "F":
                    result_AND = "N"
                elif A == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "B":
                if B == "V":
                    result_AND = "F"
                elif B == "F":
                    result_AND = "N"
                elif B == "N":
                    result_AND = "F"
            elif match_AND[0][1] == "V":
                result_AND = "F"
            elif match_AND[0][1] == "F":
                result_AND = "N"
            elif match_AND[0][1] == "N":
                result_AND = "F"
    elif match_AND[0][0] == "V":
        if match_AND[0][1] == "A":
            if A == "V":
                result_AND = "V"
            elif A == "F":
                result_AND = "F"
            elif A == "N":
                result_AND = "F"
        elif match_AND[0][1] == "B":
            if B == "V":
                result_AND = "V"
            elif B == "F":
                result_AND = "F"
            elif B == "N":
                result_AND = "F"
        elif match_AND[0][1] == "V":
            result_AND = "V"
        elif match_AND[0][1] == "F":
            result_AND = "F"
        elif match_AND[0][1] == "N":
            result_AND = "F"
    elif match_AND[0][0] == "F":
        if match_AND[0][1] == "A":
            if A == "V":
                result_AND = "F"
            elif A == "F":
                result_AND = "F"
            elif A == "N":
                result_AND = "N"
        elif match_AND[0][1] == "B":
            if B == "V":
                result_AND = "F"
            elif B == "F":
                result_AND = "F"
            elif B == "N":
                result_AND = "N"
        elif match_AND[0][1] == "V":
            result_AND = "F"
        elif match_AND[0][1] == "F":
            result_AND = "F"
        elif match_AND[0][1] == "N":
            result_AND = "N"
    elif match_AND[0][0] == "N":
        if match_AND[0][1] == "A":
            if A == "V":
                result_AND = "F"
            elif A == "F":
                result_AND = "N"
            elif A == "N":
                result_AND = "F"
        elif match_AND[0][1] == "B":
            if B == "V":
                result_AND = "F"
            elif B == "F":
                result_AND = "N"
            elif B == "N":
                result_AND = "F"
        elif match_AND[0][1] == "V":
            result_AND = "F"
        elif match_AND[0][1] == "F":
            result_AND = "N"
        elif match_AND[0][1] == "N":
            result_AND = "F"
    output = input_AND.replace(match_AND[0][0] + "^" + match_AND[0][1], result_AND, 1)
    return output


def match_OR(input_OR):
    result_OR = ""
    match_OR = re.findall('([^v])v([^v])', input_OR)
    if match_OR[0][0] == "A":
        if A == "V":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "V"
                elif A == "N":
                    result_OR = "V"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "V"
                elif B == "N":
                    result_OR = "V"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "V"
            elif match_OR[0][1] == "N":
                result_OR = "V"
        elif A == "F":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "F"
                elif A == "N":
                    result_OR = "F"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "F"
                elif B == "N":
                    result_OR = "F"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "F"
            elif match_OR[0][1] == "N":
                result_OR = "F"
        elif A == "N":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "F"
                elif A == "N":
                    result_OR = "N"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "F"
                elif B == "N":
                    result_OR = "N"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "F"
            elif match_OR[0][1] == "N":
                result_OR = "N"
    elif match_OR[0][0] == "B":
        if B == "V":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "V"
                elif A == "N":
                    result_OR = "V"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "V"
                elif B == "N":
                    result_OR = "V"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "V"
            elif match_OR[0][1] == "N":
                result_OR = "V"
        elif B == "F":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "F"
                elif A == "N":
                    result_OR = "F"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "F"
                elif B == "N":
                    result_OR = "F"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "F"
            elif match_OR[0][1] == "N":
                result_OR = "F"
        elif B == "N":
            if match_OR[0][1] == "A":
                if A == "V":
                    result_OR = "V"
                elif A == "F":
                    result_OR = "F"
                elif A == "N":
                    result_OR = "N"
            elif match_OR[0][1] == "B":
                if B == "V":
                    result_OR = "V"
                elif B == "F":
                    result_OR = "F"
                elif B == "N":
                    result_OR = "N"
            elif match_OR[0][1] == "V":
                result_OR = "V"
            elif match_OR[0][1] == "F":
                result_OR = "F"
            elif match_OR[0][1] == "N":
                result_OR = "N"
    elif match_OR[0][0] == "V":
        if match_OR[0][1] == "A":
            if A == "V":
                result_OR = "V"
            elif A == "F":
                result_OR = "V"
            elif A == "N":
                result_OR = "V"
        elif match_OR[0][1] == "B":
            if B == "V":
                result_OR = "V"
            elif B == "F":
                result_OR = "V"
            elif B == "N":
                result_OR = "V"
        elif match_OR[0][1] == "V":
            result_OR = "V"
        elif match_OR[0][1] == "F":
            result_OR = "V"
        elif match_OR[0][1] == "N":
            result_OR = "V"
    elif match_OR[0][0] == "F":
        if match_OR[0][1] == "A":
            if A == "V":
                result_OR = "V"
            elif A == "F":
                result_OR = "F"
            elif A == "N":
                result_OR = "F"
        elif match_OR[0][1] == "B":
            if B == "V":
                result_OR = "V"
            elif B == "F":
                result_OR = "F"
            elif B == "N":
                result_OR = "F"
        elif match_OR[0][1] == "V":
            result_OR = "V"
        elif match_OR[0][1] == "F":
            result_OR = "F"
        elif match_OR[0][1] == "N":
            result_OR = "F"
    elif match_OR[0][0] == "N":
        if match_OR[0][1] == "A":
            if A == "V":
                result_OR = "V"
            elif A == "F":
                result_OR = "F"
            elif A == "N":
                result_OR = "N"
        elif match_OR[0][1] == "B":
            if B == "V":
                result_OR = "V"
            elif B == "F":
                result_OR = "F"
            elif B == "N":
                result_OR = "N"
        elif match_OR[0][1] == "V":
            result_OR = "V"
        elif match_OR[0][1] == "F":
            result_OR = "F"
        elif match_OR[0][1] == "N":
            result_OR = "N"
    output = input_OR.replace(match_OR[0][0] + "v" + match_OR[0][1], result_OR, 1)
    return output

def match_IMPLICATION(input_IMPLICATION):
    result_IMPLICATION = ""
    match_IMPLICATION = re.findall('([^→])→([^→])', input_IMPLICATION)
    if match_IMPLICATION[0][0] == "A":
        if A == "V":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "F"
                elif A == "N":
                    result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "F"
                elif B == "N":
                    result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "F"
        elif A == "F":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "V"
                elif A == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "V"
                elif B == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "V"
        elif A == "N":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "V"
                elif A == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "V"
                elif B == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "V"
    elif match_IMPLICATION[0][0] == "B":
        if B == "V":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "F"
                elif A == "N":
                    result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "F"
                elif B == "N":
                    result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "F"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "F"
        elif B == "F":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "V"
                elif A == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "V"
                elif B == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "V"
        elif B == "N":
            if match_IMPLICATION[0][1] == "A":
                if A == "V":
                    result_IMPLICATION = "V"
                elif A == "F":
                    result_IMPLICATION = "V"
                elif A == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "B":
                if B == "V":
                    result_IMPLICATION = "V"
                elif B == "F":
                    result_IMPLICATION = "V"
                elif B == "N":
                    result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "V":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "F":
                result_IMPLICATION = "V"
            elif match_IMPLICATION[0][1] == "N":
                result_IMPLICATION = "V"
    elif match_IMPLICATION[0][0] == "V":
        if match_IMPLICATION[0][1] == "A":
            if A == "V":
                result_IMPLICATION = "V"
            elif A == "F":
                result_IMPLICATION = "F"
            elif A == "N":
                result_IMPLICATION = "F"
        elif match_IMPLICATION[0][1] == "B":
            if B == "V":
                result_IMPLICATION = "V"
            elif B == "F":
                result_IMPLICATION = "F"
            elif B == "N":
                result_IMPLICATION = "F"
        elif match_IMPLICATION[0][1] == "V":
            result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "F":
            result_IMPLICATION = "F"
        elif match_IMPLICATION[0][1] == "N":
            result_IMPLICATION = "F"
    elif match_IMPLICATION[0][0] == "F":
        if match_IMPLICATION[0][1] == "A":
            if A == "V":
                result_IMPLICATION = "V"
            elif A == "F":
                result_IMPLICATION = "V"
            elif A == "N":
                result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "B":
            if B == "V":
                result_IMPLICATION = "V"
            elif B == "F":
                result_IMPLICATION = "V"
            elif B == "N":
                result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "V":
            result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "F":
            result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "N":
            result_IMPLICATION = "V"
    elif match_IMPLICATION[0][0] == "N":
        if match_IMPLICATION[0][1] == "A":
            if A == "V":
                result_IMPLICATION = "V"
            elif A == "F":
                result_IMPLICATION = "V"
            elif A == "N":
                result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "B":
            if B == "V":
                result_IMPLICATION = "V"
            elif B == "F":
                result_IMPLICATION = "V"
            elif B == "N":
                result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "V":
            result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "F":
            result_IMPLICATION = "V"
        elif match_IMPLICATION[0][1] == "N":
            result_IMPLICATION = "V"
    output = input_IMPLICATION.replace(match_IMPLICATION[0][0] + "→" + match_IMPLICATION[0][1], result_IMPLICATION, 1)
    return output

def match_COIMPLICATION(input_COIMPLICATION):
    result_COIMPLICATION = ""
    match_COIMPLICATION = re.findall('([^-])-([^-])', input_COIMPLICATION)
    if match_COIMPLICATION[0][0] == "A":
        if A == "V":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "V"
                elif A == "F":
                    result_COIMPLICATION = "F"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "V"
                elif B == "F":
                    result_COIMPLICATION = "F"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "V"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
        elif A == "F":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "F"
                elif A == "F":
                    result_COIMPLICATION = "F"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "F"
                elif B == "F":
                    result_COIMPLICATION = "F"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
        elif A == "N":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "N"
                elif A == "F":
                    result_COIMPLICATION = "N"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "N"
                elif B == "F":
                    result_COIMPLICATION = "N"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
    elif match_COIMPLICATION[0][0] == "B":
        if B == "V":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "V"
                elif A == "F":
                    result_COIMPLICATION = "F"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "V"
                elif B == "F":
                    result_COIMPLICATION = "F"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "V"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
        elif B == "F":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "F"
                elif A == "F":
                    result_COIMPLICATION = "F"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "F"
                elif B == "F":
                    result_COIMPLICATION = "F"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "F"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
        elif B == "N":
            if match_COIMPLICATION[0][1] == "A":
                if A == "V":
                    result_COIMPLICATION = "N"
                elif A == "F":
                    result_COIMPLICATION = "N"
                elif A == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "B":
                if B == "V":
                    result_COIMPLICATION = "N"
                elif B == "F":
                    result_COIMPLICATION = "N"
                elif B == "N":
                    result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "V":
                result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "F":
                result_COIMPLICATION = "N"
            elif match_COIMPLICATION[0][1] == "N":
                result_COIMPLICATION = "N"
    elif match_COIMPLICATION[0][0] == "V":
        if match_COIMPLICATION[0][1] == "A":
            if A == "V":
                result_COIMPLICATION = "V"
            elif A == "F":
                result_COIMPLICATION = "F"
            elif A == "N":
                result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "B":
            if B == "V":
                result_COIMPLICATION = "V"
            elif B == "F":
                result_COIMPLICATION = "F"
            elif B == "N":
                result_COIMPLICATION = "F"
        elif match_COIMPLICATION[0][1] == "V":
            result_COIMPLICATION = "V"
        elif match_COIMPLICATION[0][1] == "F":
            result_COIMPLICATION = "F"
        elif match_COIMPLICATION[0][1] == "N":
            result_COIMPLICATION = "N"
    elif match_COIMPLICATION[0][0] == "F":
        if match_COIMPLICATION[0][1] == "A":
            if A == "V":
                result_COIMPLICATION = "F"
            elif A == "F":
                result_COIMPLICATION = "F"
            elif A == "N":
                result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "B":
            if B == "V":
                result_COIMPLICATION = "F"
            elif B == "F":
                result_COIMPLICATION = "F"
            elif B == "N":
                result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "V":
            result_COIMPLICATION = "F"
        elif match_COIMPLICATION[0][1] == "F":
            result_COIMPLICATION = "F"
        elif match_COIMPLICATION[0][1] == "N":
            result_COIMPLICATION = "N"
    elif match_COIMPLICATION[0][0] == "N":
        if match_COIMPLICATION[0][1] == "A":
            if A == "V":
                result_COIMPLICATION = "N"
            elif A == "F":
                result_COIMPLICATION = "N"
            elif A == "N":
                result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "B":
            if B == "V":
                result_COIMPLICATION = "N"
            elif B == "F":
                result_COIMPLICATION = "N"
            elif B == "N":
                result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "V":
            result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "F":
            result_COIMPLICATION = "N"
        elif match_COIMPLICATION[0][1] == "N":
            result_COIMPLICATION = "N"
    output = input_COIMPLICATION.replace(match_COIMPLICATION[0][0] + "-" + match_COIMPLICATION[0][1], result_COIMPLICATION, 1)
    return output

def match_EXOR(input_EXOR):
    result_EXOR = ""
    match_EXOR = re.findall('([^x])x([^x])', input_EXOR)
    if match_EXOR[0][0] == "A":
        if A == "V":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "F"
                elif A == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "F"
                elif B == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "F"
        elif A == "F":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "N"
                elif A == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "N"
                elif B == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "N"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "F"
        elif A == "N":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "F"
                elif A == "N":
                    result_EXOR = "N"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "F"
                elif B == "N":
                    result_EXOR = "N"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "N"
    elif match_EXOR[0][0] == "B":
        if B == "V":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "F"
                elif A == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "F"
                elif B == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "F"
        elif B == "F":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "N"
                elif A == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "N"
                elif B == "N":
                    result_EXOR = "F"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "N"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "F"
        elif B == "N":
            if match_EXOR[0][1] == "A":
                if A == "V":
                    result_EXOR = "F"
                elif A == "F":
                    result_EXOR = "F"
                elif A == "N":
                    result_EXOR = "N"
            elif match_EXOR[0][1] == "B":
                if B == "V":
                    result_EXOR = "F"
                elif B == "F":
                    result_EXOR = "F"
                elif B == "N":
                    result_EXOR = "N"
            elif match_EXOR[0][1] == "V":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "F":
                result_EXOR = "F"
            elif match_EXOR[0][1] == "N":
                result_EXOR = "N"
    elif match_EXOR[0][0] == "V":
        if match_EXOR[0][1] == "A":
            if A == "V":
                result_EXOR = "F"
            elif A == "F":
                result_EXOR = "F"
            elif A == "N":
                result_EXOR = "F"
        elif match_EXOR[0][1] == "B":
            if B == "V":
                result_EXOR = "F"
            elif B == "F":
                result_EXOR = "F"
            elif B == "N":
                result_EXOR = "F"
        elif match_EXOR[0][1] == "V":
            result_EXOR = "F"
        elif match_EXOR[0][1] == "F":
            result_EXOR = "F"
        elif match_EXOR[0][1] == "N":
            result_EXOR = "F"
    elif match_EXOR[0][0] == "F":
        if match_EXOR[0][1] == "A":
            if A == "V":
                result_EXOR = "F"
            elif A == "F":
                result_EXOR = "N"
            elif A == "N":
                result_EXOR = "F"
        elif match_EXOR[0][1] == "B":
            if B == "V":
                result_EXOR = "F"
            elif B == "F":
                result_EXOR = "N"
            elif B == "N":
                result_EXOR = "F"
        elif match_EXOR[0][1] == "V":
            result_EXOR = "F"
        elif match_EXOR[0][1] == "F":
            result_EXOR = "N"
        elif match_EXOR[0][1] == "N":
            result_EXOR = "F"
    elif match_EXOR[0][0] == "N":
        if match_EXOR[0][1] == "A":
            if A == "V":
                result_EXOR = "F"
            elif A == "F":
                result_EXOR = "F"
            elif A == "N":
                result_EXOR = "N"
        elif match_EXOR[0][1] == "B":
            if B == "V":
                result_EXOR = "F"
            elif B == "F":
                result_EXOR = "F"
            elif B == "N":
                result_EXOR = "N"
        elif match_EXOR[0][1] == "V":
            result_EXOR = "F"
        elif match_EXOR[0][1] == "F":
            result_EXOR = "F"
        elif match_EXOR[0][1] == "N":
            result_EXOR = "N"
    output = input_EXOR.replace(match_EXOR[0][0] + "x" + match_EXOR[0][1], result_EXOR, 1)
    return output

if __name__ == '__main__':
    print("### Logica ternaria 1.1 ###")
    while True:
        expression = input('> ')
        if "A" in expression and "B" not in expression:
            A = "V"
            print("Con A=V:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "F"
            print("Con A=F:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "N"
            print("Con A=N:", expression, "=", matchExpressionBetweenBrackets(expression))
        elif "B" in expression and "A" not in expression:
            B = "V"
            print("Con B=V:", expression, "=", matchExpressionBetweenBrackets(expression))
            B = "F"
            print("Con B=F:", expression, "=", matchExpressionBetweenBrackets(expression))
            B = "N"
            print("Con B=N:", expression, "=", matchExpressionBetweenBrackets(expression))
        elif "A" in expression and "B" in expression:
            A = "V"
            B = "V"
            print("Con A=V e B=V:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "V"
            B = "F"
            print("Con A=V e B=F:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "V"
            B = "N"
            print("Con A=V e B=N:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "F"
            B = "V"
            print("Con A=F e B=V:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "F"
            B = "F"
            print("Con A=F e B=F:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "F"
            B = "N"
            print("Con A=F e B=N:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "N"
            B = "V"
            print("Con A=N e B=V:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "N"
            B = "F"
            print("Con A=N e B=F:", expression, "=", matchExpressionBetweenBrackets(expression))
            A = "N"
            B = "N"
            print("Con A=N e B=N:", expression, "=", matchExpressionBetweenBrackets(expression))
        else:
            print(expression, "=", matchExpressionBetweenBrackets(expression))
